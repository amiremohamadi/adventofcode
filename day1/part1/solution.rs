use std::collections::HashSet;

fn main() {
    let mut visited = HashSet::new();
    let numbers = include_str!("input.txt")
        .lines()
        .map(|number| number.parse::<u32>().unwrap());

    for number in numbers {
        if visited.contains(&(2020 - number)) {
            println!("{}", number * (2020 - number));
            break;
        }
        visited.insert(number);
    }
}
