use regex::Regex;

fn main() {
    let re = Regex::new("^(\\d+)-(\\d+) (.): (.+)$").unwrap();
    let answer = include_str!("../input.txt")
        .lines()
        .map(|line| re.captures(line).unwrap())
        .filter(|line| {
            let string = &line[4];
            let this_char = &line[3];
            let count_this_char = string.matches(this_char).count();

            let minimum: usize = line[1].parse().unwrap();
            let maximum: usize = line[2].parse().unwrap();

            count_this_char >= minimum && count_this_char <= maximum
        })
        .count();

    println!("{}", answer);
}
